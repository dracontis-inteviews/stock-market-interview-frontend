import React, {useCallback} from 'react';
import {Input, Modal, Space} from 'antd';
import usePrice from '../../hooks/usePrice';

interface Props {
    isVisible: boolean;
    close: () => void;
    confirm: (stockId: number, price: number) => void;
    dataForUpdate: {
        stockId: number;
        price: number;
    };
}

export default function (props: Props) {
    const {isVisible, confirm, close, dataForUpdate} = props;

    const [price, _, onPriceChange] = usePrice(dataForUpdate);

    const handleOk = useCallback(() => {
        confirm(dataForUpdate.stockId, price);
        close();
    }, [dataForUpdate, price]);

    return (
        <Modal title="Create stock" visible={isVisible} onCancel={close} onOk={handleOk}>
            <Space direction="vertical" className="data-input">
                <Input
                    className="data-input"
                    placeholder="Price"
                    pattern="[0-9.]*"
                    value={price}
                    onChange={onPriceChange}
                />
            </Space>
        </Modal>
    );
}
