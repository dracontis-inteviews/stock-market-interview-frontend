import React from 'react';
import {Stock} from '../../reducers/StocksReducer';
import {Space, Table} from 'antd';
import parseISO from 'date-fns/parseISO';
import format from 'date-fns/format';

const {Column} = Table;

interface Props {
    stocks: Array<Stock>;
    deleteConfirmationDialog: (id: number) => void;
    openUpdateDialog: (id: number, price: number) => void;
}

export default function (props: Props) {
    const {stocks, deleteConfirmationDialog, openUpdateDialog} = props;
    return (
        <Table dataSource={stocks}>
            <Column title="Name" dataIndex="name" key="name"/>
            <Column title="Price" dataIndex="currentPrice" key="currentPrice"/>
            <Column
                title="Creation Date"
                key="createdAt"
                render={(_, record: Stock) => {
                    return format(parseISO(record.createdAt), "dd.MM.yyyy' at 'HH:mm:ss");
                }}
            />
            <Column
                title="Modification Date"
                key="lastUpdated"
                render={(_, record: Stock) => {
                    return format(parseISO(record.lastUpdated), "dd.MM.yyyy' at 'HH:mm:ss");
                }}
            />
            <Column
                title="Actions"
                key="actions"
                render={(_, record: Stock) => {
                    if (record.isLocked) {
                        return null;
                    }
                    return (
                        <Space size="middle" key={record.id}>
                            <a onClick={() => deleteConfirmationDialog(record.id)}>Delete</a>
                            <a onClick={() => openUpdateDialog(record.id, record.currentPrice)}>Update</a>
                        </Space>
                    );
                }}
            />
        </Table>
    );
}
