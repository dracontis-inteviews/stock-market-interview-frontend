import React, {useCallback, useState} from 'react';
import {Input, Modal, Space} from 'antd';
import './style.css';
import usePrice from '../../hooks/usePrice';

interface Props {
    isVisible: boolean;
    close: () => void;
    confirm: (name: string, price: number | undefined) => void;
}

export default function (props: Props) {
    const {isVisible, confirm, close} = props;

    const [name, setName] = useState('');
    const onNameChange = useCallback((input: any) => setName(input.target.value), [name]);
    const [price, setPrice, onPriceChange] = usePrice();

    const handleCancel = useCallback(() => {
        setName('');
        setPrice(0);
        close();
    }, []);
    const handleOk = useCallback(() => {
        confirm(name, price);
        handleCancel();
    }, [name, price]);

    return (
        <Modal title="Create stock" visible={isVisible} onCancel={handleCancel} onOk={handleOk}>
            <Space direction="vertical" className="data-input">
                <Input className="data-input" placeholder="Name" value={name} onChange={onNameChange}/>
                <Input
                    className="data-input"
                    placeholder="Price"
                    pattern="[0-9.]*"
                    value={price}
                    onChange={onPriceChange}
                />
            </Space>
        </Modal>
    );
}
