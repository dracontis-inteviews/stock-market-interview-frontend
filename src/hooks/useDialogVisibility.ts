import {useCallback, useState} from 'react';

export default function (): [boolean, () => void, () => void] {
    const [isVisible, setVisibility] = useState(false);
    const open = useCallback(() => setVisibility(true), [isVisible]);
    const close = useCallback(() => setVisibility(false), [isVisible]);

    return [isVisible, open, close];
}
