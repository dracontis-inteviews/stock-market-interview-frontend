import {useCallback, useEffect, useState} from 'react';

export default function usePrice(
    defaultPrice: { price: number } | null = null
): [number, (value: number) => void, (input: any) => void] {
    const [price, setPrice] = useState(0);
    if (defaultPrice) {
        useEffect(() => {
            setPrice(defaultPrice.price);
        }, [defaultPrice]);
    }

    const onPriceChange = useCallback(
        (input) => {
            const {value} = input.target;
            if (isEmpty(value) || isValidPositiveNumber(value)) {
                setPrice(value);
            } else {
                setPrice(price ? price : 0);
            }
        },
        [price]
    );

    return [price, setPrice, onPriceChange];
}

function isEmpty(value: string): boolean {
    return value === '';
}

function isValidPositiveNumber(value: string): boolean {
    const numberRegex = /^[0-9\\.]+$/;
    const dotRegex = /\\./;
    return numberRegex.test(value) && (value.match(dotRegex) || []).length <= 1;
}
