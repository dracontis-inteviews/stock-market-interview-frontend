import {StocksState} from './StocksReducer';

export interface RootState {
    stocksReducer: StocksState;
}
