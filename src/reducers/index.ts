import {combineReducers} from 'redux';
import StocksReducer from './StocksReducer';

export default combineReducers({
    stocksReducer: StocksReducer,
});
