import {AnyAction} from 'redux';

export const FIND_ALL_STOCKS = 'FIND_ALL_STOCKS';
export const CREATE_STOCK = 'CREATE_STOCK';
export const UPDATE_STOCK = 'UPDATE_STOCK';
export const DELETE_STOCK = 'DELETE_STOCK';

export interface Stock {
    id: number;
    name: string;
    currentPrice: number;
    isLocked: boolean;
    createdAt: string;
    lastUpdated: string;
}

export interface StocksState {
    stocks: Array<Stock>;
}

const initialState: StocksState = {
    stocks: [],
};

export default function (state: StocksState = initialState, action: AnyAction) {
    switch (action.type) {
        case FIND_ALL_STOCKS:
            const stocks = action.payload;
            return {
                ...state,
                stocks,
            };
        case UPDATE_STOCK:
            const updatedStock = action.payload;
            return {
                ...state,
                stocks: state.stocks.map((stock) => (stock.id == updatedStock.id ? updatedStock : stock)),
            };
        case DELETE_STOCK:
            const deletedStockId = action.payload;
            return {
                ...state,
                stocks: state.stocks.filter((stock) => stock.id != deletedStockId),
            };
        case CREATE_STOCK:
            const newStock = action.payload;
            return {
                ...state,
                stocks: [...state.stocks, newStock],
            };
        default:
            return initialState;
    }
}
