import axios from 'axios';
import {CREATE_STOCK, DELETE_STOCK, FIND_ALL_STOCKS, UPDATE_STOCK} from '../reducers/StocksReducer';
import {message} from 'antd';

const apiUri = '/api/stocks';

class StocksSource {
    static findAll = () => async (dispatch) => {
        try {
            const response = await axios.get(apiUri);
            dispatch({
                type: FIND_ALL_STOCKS,
                payload: response.data,
            });
        } catch (e: any) {
            console.error('Unable to fetch data', e);
            message.error(e.message);
        }
    };

    static deleteById = (id: number) => async (dispatch) => {
        try {
            await axios.delete(`${apiUri}/${id}`);
            message.success(`Stock with id ${id} was deleted`);
            dispatch({
                type: DELETE_STOCK,
                payload: id,
            });
        } catch (e: any) {
            console.error(`Unable to delete stock with id ${id}`, e);
            message.error(e.message);
        }
    };

    static update = (id: number, currentPrice: number) => async (dispatch) => {
        try {
            const response = await axios.put(`${apiUri}/${id}`, {
                currentPrice,
            });
            message.success(`Price of the stock with id ${id} was updated`);
            dispatch({
                type: UPDATE_STOCK,
                payload: response.data,
            });
        } catch (e: any) {
            console.error(`Unable to update price on record with id ${id}`, e);
            message.error(e.message);
        }
    };

    static create = (name: string, currentPrice: number | undefined) => async (dispatch) => {
        try {
            const response = await axios.post(apiUri, {
                name,
                currentPrice,
            });
            message.success('Stock successfully created');
            dispatch({
                type: CREATE_STOCK,
                payload: response.data,
            });
        } catch (e: any) {
            console.error(`Unable to create stock with name ${name}`, e);
            message.error(e.message);
        }
    };
}

export default StocksSource;
