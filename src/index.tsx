import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import {Application} from './containers/Application';
import store from './store';
import {Layout} from 'antd';
import styled from 'styled-components';
import 'antd/dist/antd.css';

const {Header} = Layout;

const Title = styled.h1`
    font-size: 1.5em;
    text-align: center;
    color: #ffffff;
`;

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Layout>
                <Header>
                    <Title>Moiseev Artem assignment</Title>
                </Header>
                <Routes>
                    <Route path={'/'} element={<Application/>}/>
                </Routes>
            </Layout>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);
