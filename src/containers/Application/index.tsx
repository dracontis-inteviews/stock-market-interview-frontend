import React, {useCallback, useEffect, useState} from 'react';
import {Button, Layout, Modal} from 'antd';
import './style.css';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../../reducers/index.types';
import StocksSource from '../../sources/StocksSource';
import StocksTable from '../../components/StocksTable';
import CreateStockDialog from '../../components/CreateStockDialog';
import UpdateStockDialog from '../../components/UpdateStockDialog';
import useDialogVisibility from '../../hooks/useDialogVisibility';

const {Content} = Layout;
const {confirm} = Modal;

export function Application() {
    const dispatch = useDispatch();
    const stocksReducer = useSelector((state: RootState) => state.stocksReducer);
    useEffect(() => {
        dispatch(StocksSource.findAll());
    }, [dispatch]);

    const [isCreateStockDialogVisible, openCreateDialog, closeCreateDialog] = useDialogVisibility();
    const onCreateDialogConfirmationClick = useCallback(
        (name: string, price: number | undefined) => {
            closeCreateDialog();
            dispatch(StocksSource.create(name, price));
        },
        [dispatch]
    );

    const [isUpdateStockDialogVisible, open, closeUpdateDialog] = useDialogVisibility();
    const [dataForUpdate, setDataForUpdate] = useState({
        stockId: 0,
        price: 0,
    });
    const openUpdateDialog = useCallback(
        (stockId: number, price: number) => {
            setDataForUpdate({
                stockId,
                price,
            });
            open();
        },
        [isUpdateStockDialogVisible, dataForUpdate]
    );
    const onUpdateDialogConfirmationClick = useCallback(
        (stockId: number, price: number) => {
            closeUpdateDialog();
            dispatch(StocksSource.update(stockId, price));
        },
        [dispatch]
    );

    const deleteConfirmationDialog = useCallback(
        (id: number) =>
            confirm({
                title: 'Are you sure want to delete this stock?',
                content: "You won't be able to revert this action",
                okText: 'Yes',
                okType: 'danger',
                cancelText: 'No',
                onOk() {
                    dispatch(StocksSource.deleteById(id));
                },
            }),
        [dispatch]
    );

    return (
        <Content className="content">
            <div className="control-buttons">
                <Button type="primary" onClick={openCreateDialog}>
                    Add stock
                </Button>
            </div>
            <CreateStockDialog
                isVisible={isCreateStockDialogVisible}
                close={closeCreateDialog}
                confirm={onCreateDialogConfirmationClick}
            />
            <UpdateStockDialog
                isVisible={isUpdateStockDialogVisible}
                close={closeUpdateDialog}
                confirm={onUpdateDialogConfirmationClick}
                dataForUpdate={dataForUpdate}
            />
            <StocksTable
                stocks={stocksReducer.stocks}
                openUpdateDialog={openUpdateDialog}
                deleteConfirmationDialog={deleteConfirmationDialog}
            />
        </Content>
    );
}
